function filtroPromedio (dir_imagen) 
   imagen =imread(dir_imagen);
   imagen_gris = escalaGrises(imagen, 1);
   imagen_D = double(imagen_gris); %% convertimos a double para hacer operaciones
   tam = 3; % filas y columnas de la matriz filtro
   
   radio = (tam - 1) / 2; % radio = numero de vecinos
   matriz = reflejarBordesImagen(imagen_D, radio);
   [f, c] = size(matriz); % Nº filas y columnas de la nueva matriz 
                     % con duplicacion de bordes
 
   peso = 9; %% para obtener el promedio

   F=[1 1 1; 1 1 1; 1 1 1]*(1/peso); % matriz filtro de 3x3
 
   % se resta y suma el radio de vecindad
   for i=(1 + radio): (f - radio)
      for j= (1 + radio): (c - radio)      
         fil=1;
         for a=(i - radio): (i + radio)
            col=1;
            for b=(j - radio): (j + radio)
         	    aux(fil, col) = matriz(a, b);
         	    col= col + 1;
            end
            
            fil = fil + 1;
         end      
         
         % multiplicamos el la matriz filtro con cada pixel y sus vecinos
         suma=0;     
         for a=1: tam     
            for b=1: tam
               suma = suma + aux(a, b) * F(a, b);   
            end
         end
         
         imagen_filtro(i - radio, j - radio) = fix(suma); % fix: trunca valores
         % Se asigna el nuevo pixel encontrado a la nueva imagen
         % imagen_filtro = imagen con filtro
      end
   end

   subplot(1, 2, 1);
   imshow(imagen_gris);
   title('Imagen Original');
   subplot(1,2,2);
   imshow(uint8(imagen_filtro)); % convertimos a uint8 para visualizarla
   title('Imagen con Filtro Promedio');